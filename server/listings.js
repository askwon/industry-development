/**
 * Listings.js - Primary Retsly API interaction module
 **/

const fs = require('fs')
const url = require('url')
const https = require('https')

// Set API variables
const vendor = 'test'
const token = process.env.RETSLY_TOKEN
if (!token) {
  throw new Error('Retsly API token not set!')
}

// Callback after a successful API call to send listings to the client
function sendData (err, res, data) {
  if (err) {
    console.log(err)
    console.log(data)
    // TODO: Display error later simply exit with failure code for now
    process.exit(1)
  }

  console.log('Data response written to output.json.')
  fs.writeFileSync('output.json', JSON.stringify(data, null, 2))
  res.end(JSON.stringify(data))
}

// Handle successful retrieval of listings data from the API
function handleResponse (data, api, res) {
  console.log(`Received response (${api.statusCode})`)
  api.on('data', (chunk) => { data += chunk })
  api.on('end', () => sendData(null, res, JSON.parse(data)))
}

function parseURI (uri) {
  const query = url.parse(uri).query
  return encodeURI(query)
}

// Call the Retsly API
function retrieveData (query, res) {
  let data = ''
  const uri = `https://rets.io/api/v1/${vendor}/listings?access_token=${token}&${query}`
  console.log(`Retrieving data for ${uri}`)
  https.get(uri, (api) => handleResponse(data, api, res))
    .on('error', (e) => sendData(e.message, null, null))
}

exports.parseURI = parseURI
exports.retrieveData = retrieveData
