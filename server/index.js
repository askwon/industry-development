/**
 * index.js - Root server script
 **/

const fs = require('fs')
const http = require('http')

const port = 1337
const listings = require('./listings.js')

// Serve various static assets to the client
function handler (req, res) {
  console.log(req.url)

  let filepath = req.url
  let contentType = 'text/html'

  // Determine file paths to serve
  if (filepath === '/') {
    filepath = '../index.html'
  } else if (filepath === '/bundle.js') {
    filepath = '../client/bundle/bundle.js'
    contentType = 'text/javascript'
  } else if (filepath === '/bundle.js.map') {
    filepath = '../client/bundle/bundle.js.map'
    contentType = 'text/javascript'
  }

  if (filepath.match('listings')) {
    console.log(req.url)
    const query = listings.parseURI(req.url)
    listings.retrieveData(query, res)
  } else {
    fs.readFile(filepath, function (err, content) {
      if (err) {
        if (err.code === 'ENOENT') {
          res.writeHead(200, { 'Content-Type': contentType })
          res.end(content, 'utf-8')
        } else {
          res.writeHead(500)
          res.end(`Sorry, check with the site admin for error: ${err.code}..`)
        }
      } else {
        res.writeHead(200, { 'Content-Type': contentType })
        res.end(content, 'utf-8')
      }
    })
  }
}

// Create the server
const server = http.createServer(handler)
server.listen(port, function () {
  console.log(`🌏 Server running on http://localhost:${port}`)
})
