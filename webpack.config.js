var path = require('path')
var webpack = require('webpack')

module.exports = {
  context: path.resolve('./client'),
  entry: [
    path.resolve('./client/app.js')
  ],
  output: {
    path: path.join('./client/bundle'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        include: [
          path.resolve('client')
        ],
        test: /\.js$/,
        query: {
          presets: ['es2015', 'stage-0']
        }
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }
    ]
  },
  plugins: [
    // Avoid publishing files when compilation fails
    new webpack.NoErrorsPlugin()
  ],
  stats: {
    // Nice colored output
    colors: true
  },
  // Create Sourcemaps for the bundle
  devtool: 'source-map'
}
