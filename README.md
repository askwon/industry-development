# Industry Development
Coding exercise for Zillow

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)

---

### Local Setup
Assuming you have Node.js (v6.x.x) and npm (v.3.x.x):

* `npm install && npm start`
* Go to `http://localhost:1337`

> `npm install` will globally install webpack and concurrently, then install local dependencies.

> `npm start` will concurrently build the client-side modules and run the server.

Alternatively, you can manually run the commands that build the client-side ES6 modules and run the server:

* In the root directory, run `webpack client/app.js`, which bundles all client-side modules into a single script in `client/bundle/bundle.js`
* Run `node server/index.js`, which will serve the application at `http://localhost:1337`