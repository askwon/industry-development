require('./styles/app.css')

import $ from 'jquery'
import Prompt from './views/Prompt.js'
import Listings from './views/Listings.js'

const Container = document.getElementsByClassName('container')[0]

function viewListings (data) {
  console.log('Received data!')
  console.dir(data)
  const ListingsView = new Listings(data, viewPrompt)

  $('.container').children().fadeOut(500, function () {
    $('.container').empty()
    $(ListingsView.render()).hide().appendTo('.container').stop().fadeIn(500)
    ListingsView.finalize()
  })
}

function viewPrompt () {
  const PromptView = new Prompt(viewListings)
  $('.container').children().fadeOut(500, function () {
    $('.container').empty()
    $(PromptView.render()).hide().appendTo('.container').stop().fadeIn(500)
    PromptView.finalize()
  })
}

const InitialPromptView = new Prompt(viewListings)
Container.insertAdjacentHTML('beforeend', InitialPromptView.render())
InitialPromptView.finalize()

