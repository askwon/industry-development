import Listing from './Listing.js'
require('../styles/listings.css')

function Listings (d, callback) {
  var data = JSON.parse(d)
  var goBack = callback
  var ListingView = new Listing(data)

  function renderError () {
    return `
      <div class="error">
        <p> Whoops! Something went wrong! </p>
        <p> (${data.bundle.message || ''}) </p>
      </div>
      <button id="return-button"> Try again </button>
    `
  }

  function renderListing () {
    const listings = data.bundle.map(listing => ListingView.render(listing)).join('')
    return `
      <div class="listings">
        <h2> Available Listings </h2>
        ${listings.length ? listings : '<p> Looks like there\'s nothing here! <p>'}
      </div>
      <button id="return-button"> Give it another go </button>
    `
  }

  function finalize () {
    document.getElementById('return-button').addEventListener('click', goBack)
    document.getElementById('container').style.display = 'block'
  }

  function render () {
    console.dir(data)
    return data.success ? renderListing() : renderError()
  }

  return {
    render,
    finalize
  }
}

export default Listings
