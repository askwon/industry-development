import $ from 'jquery'
require('../styles/prompt.css')

function Prompt (cb) {
  var callback = cb

  function submit () {
    let data = {}
    $('div.container input[type=text]').each(function () {
      data[$(this)[0].name] = $(this)[0].value
    })

    $.ajax({
      url: '/listings',
      type: 'GET',
      success: d => { callback(d) },
      data
    })
  }

  function finalize () {
    document.getElementById('prompt-button').addEventListener('click', submit)
    document.getElementById('container').style.display = 'flex'
  }

  function render () {
    return `
      <div style="display: flex; flex-direction: column;">
        <h1> Zillow Coding Exercise </h1>
        <div class="inputs">
          <label for="address">Enter Address:</label><br>
          <input type="text" name="address" placeholder="Street St.">
          <button id="prompt-button"> Submit </button>
        </div>
      </div>
    `
  }

  return {
    render,
    finalize
  }
}

export default Prompt
