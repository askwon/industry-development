import moment from 'moment'

function Listing () {
  function feature (d, name) {
    return d && d.length ? `
      <tr>
        <th rowspan="${d.length}">${name}:</th>
        ${d.map(a => `<td>${a}</td> </tr><tr>`).join('')}
      </tr>
    ` : ''
  }

  function renderDetails (d) {
    return (
      feature(d.sewer, 'Sewer') +
      feature(d.heating, 'Heating') +
      feature(d.laundry, 'Laundry') +
      feature(d.cooling, 'Cooling') +
      feature(d.taxStatus, 'Tax Status') +
      feature(d.appliances, 'Appliances') +
      feature(d.disclosures, 'Disclosures') +
      feature(d.termsOfSale, 'Terms of Sale') +
      feature(d.roadSurface, 'Road Surfaces') +
      feature(d.exteriorFeature, 'Exterior Features') +
      feature(d.telephoneService, 'Telephone Service') +
      feature(d.fireplaceFeature, 'Fireplace Features') +
      feature(d.constructionMaterials, 'Building Material')
    )
  }

  function render (listing) {
    // Construct each listing list item
    const pics = listing.media.map(pic => `<image class="home-image" src="${pic.url}"></image>`)
    const active = listing.status === 'Active'
    return `
      <div class="listing-container" style="border-left-color: ${active ? '#6DCCDA' : '#F5758C'}">
        <div class="left-container">
          $${listing.price.toLocaleString()}
          <div class="original-price"> Original Price: $${listing.originalPrice.toLocaleString()} </div>
          <div class="status" ${active
            ? 'style="color: darkcyan; background-color: mediumaquamarine"> Active'
            : 'style="color: orangered; background-color: pink"> Canceled'}
          </div>
          ${pics[0]}
        </div>
        <div class="center-container">
          <div class="address"> ${listing.address} </div>
          <i class="type"> ${listing.type} </i> 
          <div class="listDate"> Listed ${moment(listing.dateListed).fromNow()} (spent ${listing.daysOnMarket} days on the market) </div>
          <div class="details">
            <div class="details-title">
              <b> Listing Details </b>
            </div>
            <table style="width:100%">
              ${renderDetails(listing)}
            </table>
          </div>
        </div>
        <div class="right-container">
          <div class="content">
            <b> Public Remarks: </b>
            <div class="remarks">
              ${listing.publicRemarks}
            </div>
          </div>
          <div class="content">
            <b> Private Remarks: </b>
            <div class="remarks">
              ${listing.privateRemarks}
            </div>
          </div>
        </div>
      </div>
    `
  }

  return {
    render
  }
}

export default Listing
